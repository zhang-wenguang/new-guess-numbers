import org.junit.Test;

import junit.framework.Assert;

public class NGuessnumberTest {
	@Test
	//测试 generateAnswer()方法
	public void generateAnswerTest(){
		NGuessnumber c = new NGuessnumber();
		String result = c.generateAnswer();
		//检查答案数字数量是否为4
		Assert.assertEquals(result.length(),4);
		//其中是否无重复数字
		int ad = Integer.valueOf(result);
		int a1,a2,a3,a4;
		a1 = ad/1000;
		a2 = ad%1000/100;
		a3 = ad%100/10;
		a4 = ad%10;
		Assert.assertEquals(a1!=a2,true);
		Assert.assertEquals(a1!=a3,true);
		Assert.assertEquals(a1!=a4,true);
		Assert.assertEquals(a2!=a3,true);
		Assert.assertEquals(a2!=a4,true);
		Assert.assertEquals(a3!=a4,true);
		//以及数字是否都在0-9之间
		Assert.assertEquals(a1>=0 && a1<=9,true);
		Assert.assertEquals(a2>=0 && a2<=9,true);
		Assert.assertEquals(a3>=0 && a3<=9,true);
		Assert.assertEquals(a4>=0 && a4<=9,true);
	}
	@Test
	//测试 getPlayerInputTest()方法
	public void getPlayerInputTest(){
		NGuessnumber c = new NGuessnumber();
		String result = c.getPlayerInput();
		//请使用断言检查玩家猜测的数字数量是否为4。
		Assert.assertEquals(result.length(),4);
	}
	@Test
	//测试 compareGuessAnswerTest()方法
	public void compareGuessAnswerTest(){
		NGuessnumber c = new NGuessnumber();
		String cga = c.compareGuessAnswer();
		//请使用断言检查玩家猜测的数字数量是否为4。
		Assert.assertEquals(cga.length(),4);
	}
	@Test
	//测试 isWin()方法
	public void isWin(){
		NGuessnumber c = new NGuessnumber();
		String cga = c.isWin();
		//请使用断言检查玩家猜测的数字数量是否为4。
		Assert.assertEquals(cga.length(),5);
	}
}
